Flask==2.2.2
psycopg2-binary==2.9.3
SQLAlchemy==1.4.41
PyJWT==2.3.0
flask-cors==3.0.10

torch==1.13.0
torchvision==0.14.0
matplotlib==3.6.2
numpy==1.23.5
pandas==1.5.2
Pillow==9.3.0
opencv-python==4.6.0.66
cv==1.0.0
scikit-learn==1.1.3
scikit-image==0.19.3
scipy==1.9.3
tqdm==4.45.0
