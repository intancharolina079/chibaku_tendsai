from preprocessing import normalize_image
from inference import extract_model
from postprocessing import predict


def predict_image(path): # path = base64
    img = normalize_image(path)
    model = extract_model()
    
    return predict(model, img)