from sqlalchemy import (
    MetaData,
    Table,
    insert,
)
from sqlalchemy.exc import IntegrityError
from utils import run_query, get_engine
from flask import Blueprint, request, send_from_directory
import uuid

import os

image_bp = Blueprint("images", __name__, url_prefix="/images")

@image_bp.route("/banner/<path:image_extension>", methods=["GET"])
def get_banner(image_extension):

    root = os.path.join(os.path.dirname(os.path.abspath(__file__)), "images", "banner")

    return send_from_directory(root, image_extension)

@image_bp.route("/categories/<path:image_extension>", methods=["GET"])
def get_categories(image_extension):

    root = os.path.join(os.path.dirname(os.path.abspath(__file__)), "images", "categories")

    return send_from_directory(root, image_extension)

@image_bp.route("/product/top/<path:image_extension>", methods=["GET"])
def get_top(image_extension):

    root = os.path.join(os.path.dirname(os.path.abspath(__file__)), "images", "product", "top")

    return send_from_directory(root, image_extension)

@image_bp.route("/product/bottom/<path:image_extension>", methods=["GET"])
def get_bottom(image_extension):

    root = os.path.join(os.path.dirname(os.path.abspath(__file__)), "images", "product", "bottom")

    return send_from_directory(root, image_extension)

@image_bp.route("/product/footwear/<path:image_extension>", methods=["GET"])
def get_footwear(image_extension):

    root = os.path.join(os.path.dirname(os.path.abspath(__file__)), "images", "product", "footwear")

    return send_from_directory(root, image_extension)

@image_bp.route("/product/accessories/<path:image_extension>", methods=["GET"])
def get_accessories(image_extension):

    root = os.path.join(os.path.dirname(os.path.abspath(__file__)), "images", "product", "accessories")

    return send_from_directory(root, image_extension)
    
