from sqlalchemy import (
    MetaData,
    Table,
    insert,
)
from sqlalchemy.exc import IntegrityError
from utils import run_query, get_engine
from flask import Blueprint, request
import uuid


home_bp = Blueprint("home", __name__, url_prefix="/home")

@home_bp.route("/banner", methods=["GET"])
def get_banner():

    try:
        
        data = run_query("SELECT * FROM banner")

        return {"data": data}, 201

    except IntegrityError:

        return {"message": "Error, no data in banner"}, 400


@home_bp.route("/category", methods=["GET"])
def get_category():

    try:
        
        data = run_query("SELECT id,image,title FROM categories WHERE deleted_at IS NULL")

        return {
                "data": data, 
                "message": "Get Category Success"
            }, 201

    except IntegrityError:

        return {"message": "Error, no data in categories"}, 400
