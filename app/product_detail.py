from sqlalchemy import (
    MetaData,
    Table,
    insert,
)
from sqlalchemy.exc import IntegrityError
from utils import run_query, get_engine, decode_auth_token
from flask import Blueprint, request
import uuid
import jwt


details_bp = Blueprint("details", __name__, url_prefix="/products")

@details_bp.route("/<path:productId>", methods=["GET"])
def get_details(productId):

    try:

        sizes = []
        images_url = []

        data = run_query(f"SELECT * FROM product WHERE id = '{productId}';")
        
        for x in data:
            title = x['title']
            product_detail = x['description']
            images = x['image']
            size = x['size']
            price = x['price']
            category_name = x['category']


            sizes.append(size)
            images_url.append(images)

        check_category = run_query(f"SELECT id FROM categories WHERE title = '{category_name}';")
        for y in check_category:
            cateogry_id = y['id']

        return {
                "data" : { "id" : f"{productId}", "title" : title, "size" : sizes, "product_detail" : product_detail, "price" : price, "images_url" : images_url, "category_id" : cateogry_id, "category_name" : category_name }, 
                "message" : "Get Details success"
            }, 201

    except IntegrityError:

        return {"message": "Error, products not found"}, 400

cart_bp = Blueprint("cart", __name__, url_prefix="/cart")

@cart_bp.route("", methods=["POST"])
def add_to_cart():
    duplicate = False

    token = request.headers["Authentication"]

    id = request.json["id"]
    quantity = request.json["quantity"]
    size = request.json["size"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
    
    email = check_token['email']

    check_item = run_query("SELECT * FROM product")

    amount = int(quantity)
            
    for y in check_item:
        if y['id'] == f'{id}':
            if y['size'] == size:
                stock = y['stock']
                price = y['price']

                if stock < amount:

                    return {"message": "Insufficient stock"}, 400

                else:

                    check_cart = run_query(f"SELECT * FROM cart WHERE email = '{email}'")
                    for z in check_cart:
                        if z['id'] == f'{id}':
                            if z['size'] == size:
                                duplicate = True

                    if duplicate == False:
                        run_query(f"INSERT INTO cart VALUES('{email}','{id}','{size}',{price},{amount})", commit=True)


                    elif duplicate == True:

                        check_amount = run_query(f"SELECT amount FROM cart WHERE email = '{email}' AND id = '{id}' AND size = '{size}'")
                        for x in check_amount:
                            amounts = x['amount']

                        if amounts + amount > stock:

                            return {"message": "Insufficient stock"}, 400

                        else:
                            run_query(f"UPDATE cart SET amount = amount + {amount} WHERE email = '{email}' AND id = '{id}' AND size = '{size}'", commit=True)

                    return {"message": "Item added to cart, success"}, 200
            
            return {"message": "Size is unknown"}, 400
            

    return {"message": "Item is unknown"}, 400