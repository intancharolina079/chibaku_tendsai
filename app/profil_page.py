from sqlalchemy import (
    MetaData,
    Table,
    insert,
)
from sqlalchemy.exc import IntegrityError
from utils import run_query, get_engine, decode_auth_token
from flask import Blueprint, request

import uuid
import jwt

user_bp = Blueprint("user", __name__, url_prefix="/user")

@user_bp.route("", methods=["GET"])
def user_detail():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    try:

        data = run_query(f"SELECT name,email,phone_number FROM users WHERE email = '{email}'")
        for x in data:
            name = x['name']
            email = x['email']
            phone_number = x['phone_number']

        return {
                    "data" : { "name" : name, "email" : email, "phone_number" : phone_number }, 
                    "message": "Get User's Detail success"
                }, 201

    except IntegrityError:

        return {"message": "Something error, unable complete request"}, 400

@user_bp.route("/shipping_address", methods=["POST"])
def change_shipping():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']
    
    name_add = request.json['name']
    phone_number = request.json['phone_number']
    address = request.json['address']
    city = request.json['city']

    shipping_address = { "name" : name_add, "phone_number" : phone_number, "address" : address, "city" : city }

    try:

        run_query(f"UPDATE orders SET name_add = '{name_add}', phone_number = '{phone_number}', address = '{address}', city = '{city}' WHERE email = '{email}'", commit=True)

        new_address = { "name": name_add, "phone_number": phone_number, "address" : address, "city": city }
              
        return {
                    "Shipping Address" : new_address, 
                    "message": "Change Shipping Adress success"
                }, 201

    except IntegrityError:

        return {"message": "Something error, unable complete request"}, 400

@user_bp.route("/balance", methods=["POST"])
def top_up():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']
    
    amount = int(request.json["amount"])

    try:

        if amount <= 0:
            return {"message": "Please specify a positive amount"}, 400

        else:
            check_balance = run_query(f"SELECT balance FROM users WHERE email = '{email}'")
            for x in check_balance:
                balance = x['balance']

            total = balance + amount
        
            run_query(f"UPDATE users SET balance = balance + {amount} WHERE email = '{email}'", commit=True)
        
        return {
                    "Your Balance Now" : total, 
                    "message": "Top Up Balance success"
                }, 201

    except IntegrityError:

        return {"message": "Something error, unable complete request"}, 400

@user_bp.route("/balance", methods=["GET"])
def get_balance():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    try:
        
        data = run_query(f"SELECT balance FROM users WHERE email = '{email}'")
        for x in data:
            balance = x['balance']
        
        return {
                    "data" : { "balance" : balance }, 
                    "message": "Get Balance success"
                }, 201

    except IntegrityError:

        return {"message": "Something error, unable complete request"}, 400

@user_bp.route("/shipping_address", methods=["GET"])
def get_address():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    try:

        data = run_query(f"SELECT * FROM address WHERE email = '{email}'")
        for x in data:
            name = x['name']
            phone_number = x['phone_number']
            address = x['address']
            city = x['city']

        return {
                "data" : { "name" : name, "phone_number" : phone_number, "address" : address, "city" : city }, 
                "message" : "Get User's Shipping Address success"
            }, 201

    except IntegrityError:

        return {"message": "Something error, unable complete request"}, 400

@user_bp.route("/order", methods=["GET"])
def get_user_order():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    try:

        data = []
        products = []
        
        check_orders = run_query(f"SELECT * FROM orders WHERE email = '{email}'")
        for x in check_orders:

            name_add = x['name_add']
            phone_number = x['phone_number']
            address = x['address']
            city = x['city']
            shipping_method = x['shipping_method']
            created_at = x['created_at']

            product_id = x['product_id']
            details = { "quantity" : x['amount'], "size" : x['size']}
            price = x['price']

            check_name =  run_query(f"SELECT title,image FROM product WHERE id = '{product_id}'")
            for y in check_name:
                name = y['title']
                image = y['image']

            sub_data = {
                        "id" : product_id,
                        "details" : details,
                        "price" : price,
                        "image" : image,
                        "name" : name
            }

            products.append(sub_data)

        check_order_id = run_query(f"SELECT id FROM orders WHERE email = '{email}'")
        for y in check_order_id:
            order_id = y['id']

        shipping_address = { "name" : name_add, "phone_number" : phone_number, "address" : address, "city" : city }

        final_data = {
                        "id" : order_id,
                        "created_at" : created_at,
                        "products" : products,
                        "shipping_method" : shipping_method,
                        "shipping_address" : shipping_address

        }

        data.append(final_data)
        
        return {
                    "data" : data, 
                    "message": "Get User's Order success"
                }, 201

    except IntegrityError:

        return {"message": "Something error, unable complete request"}, 400