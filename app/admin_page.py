from sqlalchemy import (
    MetaData,
    Table,
    insert,
)
from sqlalchemy.exc import IntegrityError
from utils import run_query, get_engine, decode_auth_token
from flask import Blueprint, request

import uuid
import jwt
import json
import base64

get_orders_bp = Blueprint("get_orders", __name__, url_prefix="/orders")

@get_orders_bp.route("", methods=["GET"])
def get_orders():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    check_admin = run_query(f"SELECT type FROM users WHERE email = '{email}'")
    for x in check_admin:
        type = x['type']

    try:

        if type == "seller":
        
            data = []

            check_order = run_query(f"SELECT id,email,created_at,SUM(amount*price) as total FROM orders GROUP BY id,email,created_at")
            for x in check_order:
                order_id = x['id']
                user_email = x['email']
                created_at = x['created_at']
                total = x['total']

                check_user = run_query(f"SELECT id,name FROM users WHERE email = '{user_email}'")
                for y in check_user:
                    user_id = y['id']
                    user_name = y['name']

                sub_data = {
                            "id" : order_id,
                            "user_name" : user_name,
                            "created_at" : created_at,
                            "user_id" : user_id,
                            "user_email" : user_email,
                            "total" : total
                }

                data.append(sub_data)

            return {
                        "data" : data, 
                        "message": "Get Orders Success"
                    }, 201

        else:
            return {"error": "Only Admin Can Access This Page"}, 400

    except IntegrityError:

        return {"error": "Something error, unable complete request"}, 400

product_bp = Blueprint("products", __name__, url_prefix="/products")

create_product_bp = Blueprint("create_product", __name__, url_prefix="/products")

@create_product_bp.route("", methods=["POST"])
def create_product():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    check_admin = run_query(f"SELECT type FROM users WHERE email = '{email}'")
    for x in check_admin:
        role = x['type']

    product_name = request.json["product_name"]
    description = request.json["description"]
    image = request.json["images"]
    condition = request.json["condition"]
    kategori = request.json["category"]
    price = request.json["price"]

    index = image[0]
    split = index.split(",")
    images = split[1]

    try:

        if role == "seller":

            check_category = run_query(f"SELECT title FROM categories WHERE id = {kategori}")
            for x in check_category:
                category = x['title']

            check_product = run_query(f"SELECT * FROM product WHERE title = '{product_name}'")

            if check_product == []:
            
                run_query(f"INSERT INTO product(title,price,image,condition,description,category) VALUES('{product_name}',{price},'{images}','{condition}','{description}','{category}')", commit=True)

                return {
                            "message": "Product Added"
                        }, 201

            else:
                run_query(f"UPDATE product SET deleted_at = NULL WHERE title = {product_name}", commit=True)

                return {
                            "message": "Product Added"
                        }, 201


        else:
            return {"error": "Only Admin Can Access This Page"}, 400

    except IntegrityError:

        return {"error": "Something error, unable complete request"}, 400

update_product_bp = Blueprint("update_product", __name__, url_prefix="/products")

@update_product_bp.route("", methods=["PUT"])
def update_product():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    check_admin = run_query(f"SELECT type FROM users WHERE email = '{email}'")
    for x in check_admin:
        type = x['type']

    id = request.json["product_id"]

    product_name = request.json["product_name"]
    description = request.json["description"]
    image = request.json["images"]
    condition = request.json["condition"]
    kategori = request.json["category"]
    price = request.json["price"]

    images = image[0]

    try:

        if type == "seller":

            check_category = run_query(f"SELECT title FROM categories WHERE id = {kategori}")
            for x in check_category:
                category = x['title']

            run_query(f"UPDATE product SET title = '{product_name}', description = '{description}', image = '{images}', condition = '{condition}', category = '{category}', price = {price}, update_at = NOW() WHERE id = '{id}'", commit=True)

            return {
                        "message": "Product Updated"
                    }, 201

        else:
            return {"error": "Only Admin Can Access This Page"}, 400

    except IntegrityError:

        return {"error": "Something error, unable complete request"}, 400

delete_product_bp = Blueprint("delete_product", __name__, url_prefix="/products")

@delete_product_bp.route("/<path:product_id>", methods=["DELETE"])
def delete_product(product_id):

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    check_admin = run_query(f"SELECT type FROM users WHERE email = '{email}'")
    for x in check_admin:
        type = x['type']

    try:

        if type == "seller":

            check_product = run_query(f"SELECT * FROM product WHERE id = '{product_id}'")
            list_id = [d['id'] for d in check_product]

            if str(product_id) not in list_id:
                return { "error" : "Product is Unknown" }, 400

            run_query(f"UPDATE product SET deleted_at = NOW() WHERE id = '{product_id}'", commit=True)

            return { "message": "Product Deleted" }, 201

        else:
            return {"error": "Only Admin Can Access This Page"}, 400

    except IntegrityError:

        return {"error": "Something error, unable complete request"}, 400

category_bp = Blueprint("category", __name__, url_prefix="/categories")


create_category_bp = Blueprint("create_category", __name__, url_prefix="/categories")

@create_category_bp.route("", methods=["POST"])
def create_category():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    check_admin = run_query(f"SELECT type FROM users WHERE email = '{email}'")
    for x in check_admin:
        type = x['type']

    category_name = request.json["category_name"]

    try:

        if type == "seller":

            check_category = run_query(f"SELECT * FROM categories WHERE title = '{category_name}'")

            if check_category == []:
            
                run_query(f"INSERT INTO categories(title) VALUES('{category_name}')", commit=True)

                return {
                        "message": "Category Added"
                    }, 201

            else:
                run_query(f"UPDATE categories SET deleted_at = NULL WHERE title = '{category_name}'", commit=True)

                return {
                        "message": "Category Added"
                    }, 201

        else:
            return {"error": "Only Admin Can Access This Page"}, 400

    except IntegrityError:

        return {"error": "Something error, unable complete request"}, 400

update_category_bp = Blueprint("update_category", __name__, url_prefix="/categories")

@update_category_bp.route("/<path:category_id>", methods=["PUT"])
def update_category(category_id):

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    check_admin = run_query(f"SELECT type FROM users WHERE email = '{email}'")
    for x in check_admin:
        type = x['type']

    category_name = request.json["category_name"]

    try:

        if type == "seller":

            run_query(f"UPDATE categories SET title = '{category_name}' WHERE id = '{category_id}'", commit=True)

            return {
                        "message": "Category Updated"
                    }, 201

        else:
            return {"error": "Only Admin Can Access This Page"}, 400

    except IntegrityError:

        return {"error": "Something error, unable complete request"}, 400

delete_category_bp = Blueprint("delete_category", __name__, url_prefix="/categories")

@delete_category_bp.route("/<path:category_id>", methods=["DELETE"])
def delete_category(category_id):

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    check_admin = run_query(f"SELECT type FROM users WHERE email = '{email}'")
    for x in check_admin:
        type = x['type']

    try:

        if type == "seller":

            check_category = run_query(f"SELECT * FROM categories WHERE id = {category_id}")
            list_id = [d['id'] for d in check_category]

            if int(category_id) not in list_id:
                return { "error" : "Category is Unknown" }, 400

            run_query(f"UPDATE categories SET deleted_at = NOW() WHERE id = {category_id}", commit=True)

            return { "message": "Category Deleted" }, 201

        else:
            return {"error": "Only Admin Can Access This Page"}, 400

    except IntegrityError:

        return {"error": "Something error, unable complete request"}, 400

get_sales_bp = Blueprint("get_sales", __name__, url_prefix="/sales")

@get_sales_bp.route("", methods=["GET"])
def get_sales():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    check_admin = run_query(f"SELECT type FROM users WHERE email = '{email}'")
    for x in check_admin:
        type = x['type']

    try:

        if type == "seller":

            check_total = run_query("SELECT COUNT(id) as total FROM orders")
            for x in check_total:
                total = x['total']

            data = { "total" : total }

            return {
                    "data" : data, 
                    "message": "Get Total Sales Success"
                }, 201

        else:
            return {"error": "Only Admin Can Access This Page"}, 400

    except IntegrityError:

        return {"error": "Something error, unable complete request"}, 400