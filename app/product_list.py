from sqlalchemy import (
    MetaData,
    Table,
    insert,
)
from sqlalchemy.exc import IntegrityError
from utils import run_query, get_engine, decode_auth_token
from flask import Blueprint, request
import uuid
import jwt
import json

from prediction import predict_image

product_bp = Blueprint("products", __name__, url_prefix="/products")

@product_bp.route("", methods=["GET"])
def get_product():

    if "Authentication" in request.headers:
        token = request.headers["Authentication"]

        try:
            check_token = decode_auth_token(token)
        except jwt.ExpiredSignatureError:
            return {"message": "Signature expired. Please log in again"}, 400
        except jwt.InvalidTokenError:
            return {"message": "Invalid token. Please log in again"}, 400
    
        email = check_token['email']

        check_admin = run_query(f"SELECT type FROM users WHERE email = '{email}'")
        for x in check_admin:
            type = x['type']

    else :
        type = "buyer"

    if type == "seller":

        try:

            data = run_query(f"SELECT * FROM product")

            return {
                        "data": data, 
                        "message": "Get Product Admin success"
                    }, 201

        except IntegrityError:

            return {"message": "Error, products not found"}, 400

    else:

        page = request.args["page"]
        page_size = request.args["page_size"]
        sort_by = request.args["sort_by"]
        category = request.args["category"]

        if "price" not in request.args:
            price = "0,5000000"
        else:
            price = request.args["price"]

        if "condition" not in request.args:
            condition = "new,used"
        else:
            condition = request.args["condition"]

        if request.args["product_name"] == "undefined":
            product_name = ""
        else:
            product_name = request.args["product_name"]


        if sort_by == "Price a_z":
            sort = "ASC"
        elif sort_by == "Price z_a":
            sort = "DESC"

        x = category.split(",")

        if len(x) == 1:
            category_id = x[0]
        elif len(x) > 1:
            category_id = x[0]
            x.pop(0)

            for y in x:
                category_id += f" OR id = {y}"

        prices = price.split(",")
        low = prices[0]
        high = prices[1]

        a = condition.split(",")

        if len(a) == 1:
            kondisi = f"'{a[0]}'"
        elif len(a) > 1:
            kondisi = f"'{a[0]}'"
            a.pop(0)

            for b in a:
                kondisi += f" OR condition = '{b}'"

        try:

            pages = (int(page)-1) * int(page_size)

            check_title = run_query(f"SELECT title FROM categories WHERE id = {category_id} AND deleted_at IS NULL;")
            title = [d['title'] for d in check_title]

            if len(title) == 1:
                categories = f"'{title[0]}'"
            elif len(title) > 1:
                categories = f"'{title[0]}'"
                title.pop(0)

                for z in title:
                    categories += f" OR category = '{z}'"


            data = run_query(f"SELECT id,image,title,price FROM product WHERE title like '%{product_name}%' AND (category = {categories}) AND (condition = {kondisi}) AND price >= {low} AND price <= {high} AND stock > 0 AND deleted_at IS NULL GROUP BY id,image,title,price ORDER BY price {sort} LIMIT {page_size} OFFSET {pages};")
            
            rows = len(data)

            return {
                    "data": data, 
                    "total_rows" : rows, 
                    "message": "Get Product success"
                }, 201

        except IntegrityError:

            return {"message": "Error, products not found"}, 400

category_bp = Blueprint("category", __name__, url_prefix="/categories")

@category_bp.route("", methods=["GET"])
def get_category():

    if "Authentication" in request.headers:
        token = request.headers["Authentication"]

        try:
            check_token = decode_auth_token(token)
        except jwt.ExpiredSignatureError:
            return {"message": "Signature expired. Please log in again"}, 400
        except jwt.InvalidTokenError:
            return {"message": "Invalid token. Please log in again"}, 400
    
        email = check_token['email']

        check_admin = run_query(f"SELECT type FROM users WHERE email = '{email}'")
        for x in check_admin:
            type = x['type']

    else :
        type = "buyer"

    if type == "seller":

        try:

            data = run_query("SELECT id,title FROM categories")

            return {
                    "data": data, 
                    "message": "Get Category success"
                }, 201

        except IntegrityError:

            return {"message": "Error, products not found"}, 400

    else:

        try:

            data = run_query("SELECT id,title FROM categories WHERE deleted_at IS NULL")

            return {
                    "data": data, 
                    "message": "Get Category success"
                }, 201

        except IntegrityError:

            return {"message": "Error, products not found"}, 400

@product_bp.route("/search_image", methods=["POST"])
def get_searchimage():

    image = request.json["image"]

    try:

        category = predict_image(image)
        category += 1

        return {
                "category": int(category), 
                "message": "Get Category success"
            }, 201

    except IntegrityError:

        return {"message": "Error, products not found"}, 400