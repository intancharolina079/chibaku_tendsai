from __future__ import print_function

from initialization_model import FashionMNISTModelV2

import argparse

import torch

class FashionMNISTModelV2:
    pass

def main():
    # for training settings
    parser = argparse.ArgumentParser(description='PyTorch Project')
    parser.add_argument('--seed', type=int, default=1, metavar='S',help='random seed (default: 1)')
    parser.add_argument('--save-model', action='store_true', default=False, help='For Saving the current Model')

    args = parser.parse_args()

    torch.manual_seed(args.seed)

    device = "cuda" if torch.cuda.is_available() else "cpu"

    themodel = torch.load("sc_final_project/model.pth", map_location="cpu")

    model = FashionMNISTModelV2().to(device)

if __name__ == "__main__":
    FashionMNISTModelV2
    main