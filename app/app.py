from sqlalchemy import (
    Column,
    Integer,
    MetaData,
    String,
    Table,
    inspect,
    create_engine,
    insert,
    select,
    text,
    func,
    ForeignKey,
    Float,
    DateTime,
    Identity,
)

import uuid

from sqlalchemy.exc import IntegrityError
from utils import get_engine, run_query
from flask import Flask, request

from image import image_bp
from home import home_bp
from auth import signup_bp, signin_bp
from product_list import product_bp, category_bp
from product_detail import details_bp, cart_bp
from cart import address_bp, shipping_bp, order_bp
from profil_page import user_bp
from admin_page import get_orders_bp, create_product_bp, update_product_bp, delete_product_bp, create_category_bp, update_category_bp, delete_category_bp, get_sales_bp

from flask_cors import CORS

def create_app():
    app = Flask(__name__)

    CORS(app)

    engine = get_engine()
    if not inspect(engine).has_table("users"):
        meta = MetaData()
        Table(
            "users",
            meta,
            Column("id", Integer, primary_key=True),
            Column("name", String, nullable=False),
            Column("email", String, nullable=False),
            Column("password", String, nullable=False),
            Column("phone_number", String, nullable=False),
            Column("type", String, nullable=False),
            Column("token", String),
            Column("balance", Integer, nullable=False, server_default="0"),
            Column("reguler", Integer, nullable=False, server_default="0"),
            Column("next_day", Integer, nullable=False, server_default="0"),
            Column("created_at", DateTime, nullable=False, server_default=func.now()),
            Column("update_at", DateTime),
            Column("deleted_at", DateTime),
        )
        meta.create_all(engine)

    #Insert Admin into Table
    run_query(f"INSERT INTO users(name,email,password,phone_number,type) VALUES('Admin','admin@gmail.com','Admin123','085253545556','seller') ON CONFLICT DO NOTHING", commit=True)
    #Insert Admin into Table

    if not inspect(engine).has_table("address"):
        meta = MetaData()
        Table(
            "address",
            meta,
            Column("id", Integer, primary_key=True),
            Column("email", String),
            Column("name", String),
            Column("phone_number", String),
            Column("address", String),
            Column("city", String)
        )
        meta.create_all(engine)

    #Insert Address into Table
    run_query(f"INSERT INTO address VALUES(1,'blackcity@gmail.com','Kantor','085212345678','Jln. in aja dulu, lama lama juga nyaman','Jaksel') ON CONFLICT DO NOTHING", commit=True)
    #Insert Address into Table

    if not inspect(engine).has_table("cart"):
        meta = MetaData()
        Table(
            "cart",
            meta,
            Column("email", String),
            Column("id", String),
            Column("size", String),
            Column("price", Integer),
            Column("amount", Integer),
        )
        meta.create_all(engine)

    if not inspect(engine).has_table("orders"):
        meta = MetaData()
        Table(
            "orders",
            meta,
            Column("id", String),
            Column("email", String),
            Column("product_id", String),
            Column("size", String),
            Column("price", Integer),
            Column("amount", Integer),
            Column("name_add", String),
            Column("phone_number", String),
            Column("address", String),
            Column("city", String),
            Column("shipping_method", String),
            Column("created_at", DateTime)
        )
        meta.create_all(engine)

    if not inspect(engine).has_table("banner"):
        meta = MetaData()    
        Table(
            "banner",
            meta,
            Column("id", String, nullable=False),
            Column("image", String, nullable=False),
            Column("title", String, primary_key=True)
        )
        meta.create_all(engine)

    #Insert Banner into Table
    banner1 = uuid.uuid4()
    run_query(f"INSERT INTO banner VALUES('{banner1}','/images/banner/Banner1.jpeg','Banner Cover') ON CONFLICT DO NOTHING", commit=True)
    
    banner2 = uuid.uuid4()
    run_query(f"INSERT INTO banner VALUES('{banner2}','/images/banner/Banner2.jpeg','Banner Significant Event') ON CONFLICT DO NOTHING", commit=True)
    #Insert Banner into Table

    if not inspect(engine).has_table("categories"):
        meta = MetaData()    
        Table(
            "categories",
            meta,
            Column("id", Integer),
            Column("image", String),
            Column("title", String, primary_key=True),
            Column("created_at", DateTime, nullable=False, server_default=func.now()),
            Column("update_at", DateTime),
            Column("deleted_at", DateTime),
        )
        meta.create_all(engine)

    #Insert Categories into Table
    # category1 = uuid.uuid4()
    run_query(f"INSERT INTO categories VALUES(1,'/images/categories/top.jpeg','TOP',current_timestamp,NULL,NULL) ON CONFLICT DO NOTHING", commit=True)
    # category2 = uuid.uuid4()
    run_query(f"INSERT INTO categories VALUES(2,'/images/categories/bottom.jpeg','BOTTOM',current_timestamp,NULL,NULL) ON CONFLICT DO NOTHING", commit=True)
    # category3 = uuid.uuid4()
    run_query(f"INSERT INTO categories VALUES(3,'/images/categories/footwear.jpeg','FOOTWEAR',current_timestamp,NULL,NULL) ON CONFLICT DO NOTHING", commit=True)
    # category4 = uuid.uuid4()
    run_query(f"INSERT INTO categories VALUES(4,'/images/categories/accessories.jpeg','ACCESSORIES',current_timestamp,NULL,NULL) ON CONFLICT DO NOTHING", commit=True)
    run_query(f"INSERT INTO categories VALUES(5,'/images/categories/sets.jpg','SETS',current_timestamp,current_timestamp,current_timestamp) ON CONFLICT DO NOTHING", commit=True)
    #Insert Categories into Table

    if not inspect(engine).has_table("product"):
        meta = MetaData()    
        Table(
            "product",
            meta,
            Column("id", String),
            Column("category", String, nullable=False),
            Column("type", String),
            Column("title", String, nullable=False),
            Column("size", String),
            Column("price", Integer, server_default="0"),
            Column("stock", Integer, server_default="0"),
            Column("image", String, nullable=False),
            Column("condition", String, nullable=False),
            Column("description", String, nullable=False),
            Column("created_at", DateTime, nullable=False, server_default=func.now()),
            Column("update_at", DateTime),
            Column("deleted_at", DateTime),
        )
        meta.create_all(engine)

    #Insert Product into Table
    product1 = uuid.uuid4()
    run_query(f"INSERT INTO product VALUES('{product1}','TOP','tshirt','Tshirt Baby Bus Colour Black','M',150000,5,'/images/product/top/baju_baby-bus.jpg','new','Kids Tshirt Cartoon Baby Bus',current_timestamp,NULL,NULL) ON CONFLICT DO NOTHING", commit=True)
    product2 = uuid.uuid4()
    run_query(f"INSERT INTO product VALUES('{product2}','FOOTWEAR','shoes','Vans Black n White','42',375000,2,'/images/product/footwear/sepatu_vans.jpg','new','Original Vans Shoes Official',current_timestamp,NULL,NULL) ON CONFLICT DO NOTHING", commit=True)
    product3 = uuid.uuid4()
    run_query(f"INSERT INTO product VALUES('{product3}','BOTTOM','jeans','Jeans Denim Man Black','XL',250000,11,'/images/product/bottom/jeans_denim-man.jpg','used','Denim Jeans Official Used For 3 Months',current_timestamp,NULL,NULL) ON CONFLICT DO NOTHING", commit=True)
    product4 = uuid.uuid4()
    run_query(f"INSERT INTO product VALUES('{product4}','ACCESSORIES','watch','Rolex Watch Ruby For Men','39mm',1200000,1,'/images/product/accessories/watch_rolex-ruby-man.jpg','new','Original Rolex Watch Ruby Series For Men',current_timestamp,NULL,NULL) ON CONFLICT DO NOTHING", commit=True)
    #Insert Product into Table

    blueprints = [image_bp, home_bp, signup_bp, signin_bp, product_bp, category_bp, details_bp, cart_bp, address_bp, shipping_bp, order_bp, user_bp, get_orders_bp, create_product_bp, update_product_bp, delete_product_bp, create_category_bp, update_category_bp, delete_category_bp, get_sales_bp]

    for bp in blueprints:
        app.register_blueprint(bp)

    return app


app = create_app()
app.run()
