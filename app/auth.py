from sqlalchemy import (
    MetaData,
    Table,
    insert,
)
from sqlalchemy.exc import IntegrityError
from utils import run_query, get_engine, encode_auth_token
from flask import Blueprint, request
import uuid

signup_bp = Blueprint("sign-up", __name__, url_prefix="/sign-up")

@signup_bp.route("", methods=["POST"])
def sign_up():
    username = request.json["name"]
    email = request.json["email"]
    phone_number = request.json["phone_number"]
    password = request.json["password"]

    lower = any(low.islower() for low in password)
    upper = any(upp.isupper() for upp in password)
    digit = any(dig.isdigit() for dig in password)

    users = Table("users", MetaData(bind=get_engine()), autoload=True)

    if len(password) < 8:
        return {"message": "Password must contain at least 8 characters"}, 400

    if lower == False:
        return {"message": "Password must contain a lowercase letter"}, 400

    if upper == False:
        return {"message": "Password must contain an uppercase letter"}, 400

    if digit == False:
        return {"message": "Password must contain a number"}, 400

    check_name = run_query("SELECT name FROM users")

    for x in check_name:
        if x['name'] == username:
            return {"message": f"Username {username} already exists"}, 409

    try:
        run_query(f"INSERT INTO users(name, password, email, phone_number, type, created_at) VALUES('{username}','{password}','{email}','{phone_number}','buyer',current_timestamp)", commit=True)

        return {"message": "success, user created"}, 201

    except IntegrityError:

        return {"message" : "error, user not created"}, 400



signin_bp = Blueprint("sign-in", __name__, url_prefix="/sign-in")

@signin_bp.route("", methods=["POST"])
def sign_in():
    email = request.json["email"]
    password = request.json["password"]

    check_user = run_query("SELECT email, password, name, phone_number, type FROM users")

    for x in check_user:
        if x['email'] == email:
            if x['password'] == password:
                name = x['name']
                phone_number = x['phone_number']
                type = x['type']
                
                data = { "email" : email }
                token = encode_auth_token(data)
                run_query(f"UPDATE users SET token = '{token}', update_at = current_timestamp WHERE email = '{email}'", commit=True)

                return {
                    "user_information": { "name" : name, "email" : email, "phone_number" : phone_number, "type" : type }, 
                    "token" : token, 
                    "message": "Login success"
                }, 201

    return {"message": "Email or password is incorrect"}, 401

