from initialization_model import FashionMNISTModelV2
import torch
from os.path import join, dirname, realpath

def extract_model():
    model = FashionMNISTModelV2(input_shape=1, hidden_units=10, output_shape=10)
    PATH = join(dirname(realpath(__file__)), "model.pth")
    model.load_state_dict(torch.load(PATH))

    return model