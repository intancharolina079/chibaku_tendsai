import os
from sqlalchemy import create_engine, text
from sqlalchemy.exc import IntegrityError

import jwt
import datetime


def get_engine():
    """Creating SQLite Engine to interact"""

    engine_uri = "postgresql+psycopg2://{}:{}@{}:{}/{}".format(
        os.environ["POSTGRES_USER"],
        os.environ["POSTGRES_PASSWORD"],
        os.environ["POSTGRES_HOST"],
        os.environ["POSTGRES_PORT"],
        os.environ["POSTGRES_DB"],
    )
    return create_engine(engine_uri, future=True)


def run_query(query, commit: bool = False):

    engine = get_engine()
    if isinstance(query, str):
        query = text(query)

    with engine.connect() as conn:
        if commit:
            conn.execute(query)
            conn.commit()
        else:
            return [dict(row) for row in conn.execute(query)]

def encode_auth_token(data):

    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1),
            'iat': datetime.datetime.utcnow(),
            'sub': data
        }
        return jwt.encode(
            payload,
            "a1f3cddbc411411dbfd195db1f94d182",
            algorithm="HS256"
        )
    except Exception as e:
        return e

def decode_auth_token(auth_token):
    
    payload = jwt.decode(auth_token, "a1f3cddbc411411dbfd195db1f94d182", algorithms=["HS256"])
    return payload['sub']
