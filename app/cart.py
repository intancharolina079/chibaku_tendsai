from sqlalchemy import (
    MetaData,
    Table,
    insert,
)
from sqlalchemy.exc import IntegrityError
from utils import run_query, get_engine, decode_auth_token
from flask import Blueprint, request
from product_detail import cart_bp

import uuid
import jwt

@cart_bp.route("", methods=["GET"])
def get_cart():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    try:

        data = []

        check_cart = run_query(f"SELECT * FROM cart WHERE email = '{email}'")
        for x in check_cart:
            id = x['id']
            details = { "quantity" : x['amount'], "size" : x['size']}
            price = x['price']

            check_name =  run_query(f"SELECT title,image FROM product WHERE id = '{id}'")
            for y in check_name:
                name = y['title']
                image = y['image']

            sub_data = {
                        "id" : id,
                        "details" : details,
                        "price" : price,
                        "image" : image,
                        "name" : name
            }

            data.append(sub_data)

        return {
                    "data" : data, 
                    "message": "Get User's Carts success"
                }, 201

    except IntegrityError:

        return {"message": "Something error, unable complete request"}, 400

address_bp = Blueprint("shipping_address", __name__, url_prefix="/user")

@address_bp.route("/shipping_address", methods=["GET"])
def get_address():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    try:

        data = run_query(f"SELECT * FROM address WHERE email = '{email}'")
        for x in data:
            name = x['name']
            phone_number = x['phone_number']
            address = x['address']
            city = x['city']

        return {
                "data" : { "name" : name, "phone_number" : phone_number, "address" : address, "city" : city }, 
                "message" : "Get User's Shipping Address success"
            }, 201

    except IntegrityError:

        return {"message": "Something error, unable complete request"}, 400

shipping_bp = Blueprint("shipping_price", __name__, url_prefix="/shipping_price")

@shipping_bp.route("", methods=["GET"])
def get_shipping_price():

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    try:

        check_cart = run_query(f"SELECT * FROM cart WHERE email = '{email}'")

        if check_cart == []:
            return {"message": "Cart is empty"}, 400        

        check_total = run_query(f"SELECT SUM(amount*price) as total FROM cart WHERE email = '{email}'")
        for z in check_total:
            total = z['total']

        if total < 200000:
            reguler = total * 15 / 100
        
        if total >= 200000:
            reguler = total * 20 / 100

        if total < 300000:
            next_day = total * 20 / 100

        if total >= 300000:
            next_day = total * 25 / 100

        run_query(f"UPDATE users SET reguler = {reguler}, next_day = {next_day} WHERE email = '{email}'", commit=True)

        data = []

        data_reguler = { "name" : "reguler", "price" : reguler }
        data_next_day = { "name" : "next_day", "price" : next_day }

        data.append(data_reguler)
        data.append(data_next_day)

        return {
                "data" : data, 
                "message" : "Get Shipping Price Success"
            }, 201

    except IntegrityError:

        return {"message": "Something error, unable complete request"}, 400

order_bp = Blueprint("create_order", __name__, url_prefix="/order")

@order_bp.route("", methods=["POST"])
def create_order():

    token = request.headers["Authentication"]

    shipping_method = request.json["shipping_method"]
    shipping_address = request.json["shipping_address"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    try:

        check_balance = run_query(f"SELECT balance,{shipping_method} FROM users WHERE email = '{email}'")
        for x in check_balance:
            balance = x['balance']
            shipping = x[f'{shipping_method}']

        check_total = run_query(f"SELECT SUM(amount*price) as total FROM cart WHERE email = '{email}'")
        for z in check_total:
            sub_total = z['total']

        total = sub_total + shipping

        if total > balance:
            return {
                "message" : "Insufficient Balance",
                "Total" : f"{sub_total} + {shipping}",
                "Balance" : balance
            }, 400

        else:

            name_add = shipping_address['name']
            phone_number = shipping_address['phone_number']
            address = shipping_address['address']
            city = shipping_address['city']

            order_id = uuid.uuid4()

            run_query(f"UPDATE users SET balance = balance - {total} WHERE email = '{email}'", commit=True)

            update = run_query(f"SELECT * FROM cart WHERE email = '{email}'")
            for y in update:
                id = y['id']
                size = y['size']
                amount = y['amount']
                price = y['price']

                run_query(f"UPDATE product SET stock = stock - {amount} WHERE id = '{id}' AND size = '{size}'", commit=True)
                run_query(f"DELETE FROM cart WHERE email = '{email}' AND id = '{id}' AND size = '{size}'", commit=True)
                run_query(f"INSERT INTO orders(id,email,product_id,size,price,amount,name_add,phone_number,address,city,shipping_method,created_at) VALUES('{order_id}','{email}','{id}','{size}',{price},{amount},'{name_add}','{phone_number}','{address}','{city}','{shipping_method}',current_timestamp)", commit=True)

            return {
                    "Shipping Address" : shipping_address,
                    "message" : "Order success"
                }, 201

    except IntegrityError:

        return {"message": "Something error, unable complete request"}, 400

@cart_bp.route("/<path:cart_id>", methods=["DELETE"])
def delete_cart(cart_id):

    token = request.headers["Authentication"]

    try:
        check_token = decode_auth_token(token)
    except jwt.ExpiredSignatureError:
        return {"message": "Signature expired. Please log in again"}, 400
    except jwt.InvalidTokenError:
        return {"message": "Invalid token. Please log in again"}, 400
   
    email = check_token['email']

    try:

        check_cart = run_query(f"SELECT * FROM cart WHERE email = '{email}' AND id = '{cart_id}'")
        list_id = [d['id'] for d in check_cart]

        if check_cart == [{}]:
            return { "message": "Cart is empty" }, 400

        if str(cart_id) not in list_id:
            return { "message" : "No such item in your cart" }, 400

        run_query(f"DELETE FROM cart WHERE email = '{email}' AND id = '{cart_id}'", commit=True)

        return { "message": "Item deleted from cart, success" }, 201

    except IntegrityError:

        return {"message": "Something error, unable complete request"}, 400

